# MSPL 2018-2019 : Baills & Firsov
University project for the MSPL course. The purpose of this work is to identify key factors that influence the quantity of reviews for applications published on google play store.

## The Dataset
We are going to use a 2mb dataset protected by CCA 3.0 licence which can be found on [kaggle.](https://www.kaggle.com/lava18/google-play-store-apps/version/6#googleplaystore_user_reviews.csv)

# Project approach
To answer the question we asked ourselves the following questions :
1. How is download count related to review count ?
2. Does  high rating mean a high number of reviews ?
3. Are certain categories more likely to be reviewed ?
4. Is the type of applications (free or paid) related to review count ?
5. For paid applications, does the price impact the review count ?

We excluded certains attributes of the dataset because we concluded they were either irrevelant to the review count or lacked precision (no time evolution for update etc...).
